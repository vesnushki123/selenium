import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class Logintest {
    public String login = "kkatekoss@gmail.com";
    @Test
    public void Logintest()  {
        System.setProperty("webdriver.gecko.driver","C:\\Program Files\\Java\\Selenium\\geckodriver.exe");

        WebDriver driver = new FirefoxDriver();
        driver.get("https://gmail.com");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        driver.findElement(By.name("identifier")).clear();
        driver.findElement(By.name("identifier")).sendKeys("kkatekoss@gmail.com");
        driver.findElement(By.cssSelector("span.RveJvd.snByac")).click();
        new WebDriverWait(driver,10)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("password")));
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("Kkate123");
        driver.findElement(By.cssSelector("span.RveJvd.snByac")).click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("span.gb_bb.gbii")).click();
        WebElement expected = driver.findElement(By.xpath("//div[@class='gb_Db']"));
        assertEquals(login, expected.getText());


       driver.quit();
    }

}


