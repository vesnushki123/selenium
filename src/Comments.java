import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class Comments {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Java\\Selenium\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://commentssprintone.azurewebsites.net/Editor/NewComment");
    }

    @AfterMethod
    public void Close() {
        driver.quit();
    }


    @Test(groups = {"NewCommentTextField"})

    public void CheckWorkingOfNewlink() {
        driver.get("http://commentssprintone.azurewebsites.net/");
        driver.findElement(By.id("newbutton")).click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.commentprop:nth-child(2) > div:nth-child(1) > label:nth-child(1)")));

    }

    @Test(groups = {"NewCommentTextField"})
    public void сheckEmptyNewCommentFieldByDefault() {
        String expected = "";
        WebElement actual = driver.findElement(By.id("Text"));
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NewCommentTextField"})
    public void сheckRequiredCommentTextField() {
        driver.findElement(By.xpath("//input[@value='Save']")).click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        String expected = "The Comment Text field is required.";
        WebElement actual = driver.findElement(By.cssSelector(".field-validation-error > span:nth-child(1)"));
        Assert.assertEquals(expected, actual.getText());
    }

    @Test(groups = {"NewCommentTextField"})
    public void сommentTExtFieldTakesMoreThanRequiredSymbols() {
        driver.findElement(By.id("Text")).sendKeys("Lorem ipsum dolor sit amet, consectetur adipisciww");
        driver.findElement(By.id("Number")).click();
        String expected = "The maximum length of Comment Text field is 50 characters";
        WebElement actual = driver.findElement(By.cssSelector(".field-validation-error > span:nth-child(1)"));
        Assert.assertEquals(expected, actual.getText());
    }

    @Test(groups = {"NewCommentTextField"})
    public void сommentFieldNotTakesInvalidValues() {
        driver.findElement(By.id("Text")).sendKeys("!#@$%@^@&@*");
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        String expected = "The Comment Text field should contain alphanumeric characters only";
        WebElement actual = driver.findElement(By.id("errorfield"));
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NewCommentTextField"})
    public void сheckUniqueComment() throws InterruptedException {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        driver.get("http://commentssprintone.azurewebsites.net/");
        driver.findElement(By.id("newbutton")).click();
        driver.findElement(By.id("Text")).clear();
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        String expected = "Comment already exists";
        WebElement actual = driver.findElement(By.id("errorfield"));
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NumberField"})
    public void сheckEmptyNumberFieldByDefault() {
        driver.findElement(By.id("Number")).sendKeys("12345");
        WebElement actual = driver.findElement(By.id("Number"));
        String expected = "";
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NumberField"})
    public void сheckMaxValue() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("12345");
        driver.findElement(By.xpath("//input[@value='Save']")).click();
        WebElement actual = driver.findElement(By.id("errorfield"));
        String expected = "The Number field should contain value from 0 to 999";
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NumberField"})
    public void сheckValidation() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("@#$@%^&@");
        driver.findElement(By.xpath("//input[@value='Save']")).click();
        WebElement actual = driver.findElement(By.id("errorfield"));
        String expected = "Number field should contains only digits";
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"NumberField"})
    public void сheckUniqueName() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("123");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        driver.get("http://commentssprintone.azurewebsites.net/");
        driver.findElement(By.id("newbutton")).click();
        driver.findElement(By.id("Text")).clear();
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("123");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        String expected = "Comment already exists";
        WebElement actual = driver.findElement(By.id("errorfield"));
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"Checkbox"})
    public void сheckThatCheckBoxIsSelected() {
        boolean actual = driver.findElement(By.id("Active")).isSelected();
        boolean expected = true;
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"Checkbox"})
    public void сheckThatCheckBoxCanBeUnchecked() {
        driver.findElement(By.id("Active")).click();
        boolean actual = driver.findElement(By.id("Active")).isSelected();
        boolean expected = false;
        if (actual == false) {
            Assert.assertEquals(actual, expected);
        }
    }

    @Test(groups = {"Categories"})
    public void requiredCategory() {
        driver.findElement(By.id("Text")).sendKeys("TestComments");
        driver.findElement(By.id("Number")).sendKeys("777");
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        WebElement actual = driver.findElement(By.id("errorfield"));
        String expected = "Please, select at least one category";
        Assert.assertEquals(actual.getText(), expected);
    }
    @Test(groups = {"Categories"})
    public void forAllCategoriesTransferToSelected()  {
        driver.findElement(By.name("AllSelect")).click();
        int q = 1;
        List<WebElement> webElementList2 = new ArrayList<>();
        for (int i = 0; i < 6;i++){
            WebElement current = driver.findElement(By.cssSelector("#selectedCategories > div:nth-child("+ q +") > span"));
            webElementList2.add(current);
            q++;
        }

        String[] mass = {"Cat0","Cat1","Cat2","Cat3","Cat4","Cat5"};

        for (int i = 0;i < webElementList2.size();i++){
            Assert.assertTrue(webElementList2.get(i).getText().equals(mass[i]));
        }
    }
    @Test(groups = {"Categories"})
    public void forAllCategoriesTransferToAvailable() {
        driver.findElement(By.name("AllSelect")).click();
        driver.findElement(By.name("AllUnSelectBtn")).click();
        int q = 1;
        List<WebElement> webElementList2 = new ArrayList<>();
        for (int i = 0; i < 6;i++){
            WebElement current = driver.findElement(By.cssSelector("#alvailablecategories > div:nth-child("+ q +") > span"));
            webElementList2.add(current);
            q++;
        }

        String[] mass = {"Cat0","Cat1","Cat2","Cat3","Cat4","Cat5"};

        for (int i = 0;i < webElementList2.size();i++){
            Assert.assertTrue(webElementList2.get(i).getText().equals(mass[i]));
        }
    }

    @Test(groups = {"Refresh"})
    public void refreshFunctional() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.cssSelector("#editor-navigation > a")).click();
        WebElement actual = driver.findElement(By.id("Text"));
        String expected = "";
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"SaveLink"})
    public void сheckNewCommentAdding() {
        driver.findElement(By.id("Text")).sendKeys("TestComments");
        driver.findElement(By.id("Number")).sendKeys("777");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        driver.get("http://commentssprintone.azurewebsites.net/");
        driver.findElement(By.cssSelector("#main > div > div:nth-child(10) > form > table > tfoot > tr > td > a:nth-child(3)")).click();
        WebElement actual = driver.findElement(By.className("textcolumn"));
        String expected = "TestComments";
        Assert.assertEquals(actual.getText(), expected);
    }

    @Test(groups = {"SaveLink"})
    public void userStayOnNewPageAfterSaving() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("567");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector(".buttonAsLink")).click();
        String actual = driver.getTitle();
        String expected = "Editor";
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"SaveAndReturnLink"})
    public void userRedirectsToIndexPage() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("57");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector("#editor-navigation > input:nth-child(3)")).click();
        String actual = driver.getTitle();
        String expected = "Index";
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"SaveAndReturnLink"})
    public void saveAndReturnAddingComment() {
        driver.findElement(By.id("Text")).sendKeys("TestComment");
        driver.findElement(By.id("Number")).sendKeys("57");
        driver.findElement(By.id("Categories")).click();
        driver.findElement(By.name("CurSelect")).click();
        driver.findElement(By.cssSelector("#editor-navigation > input:nth-child(3)")).click();
        driver.get("http://commentssprintone.azurewebsites.net/");
        driver.findElement(By.cssSelector("#main > div > div:nth-child(10) > form > table > tfoot > tr > td > a:nth-child(3)")).click();
        WebElement actual = driver.findElement(By.className("textcolumn"));
        String expected = "TestComment";
        Assert.assertEquals(actual.getText(), expected);
    }
    @Test(groups = {"ReturnLink"})
    public void returnlink() {
        driver.findElement(By.cssSelector("#logindisplay > a")).click();
        String actual = driver.getTitle();
        String expected = "Index";
        Assert.assertEquals(actual, expected);
    }

}






